package com.company;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * MovieRatings class
 */
public class MovieRatings {
    // Hadoop configuration
    Configuration configuration;
    // data directory
    String dataDir;
    // results directory
    String resultsDir;
    // MapReduce Job
    Job ratingsJob;

    /**
     * Constructor for MovieRatings
     *
     * @param dataDir
     * @param resultsDir
     */
    public MovieRatings(String dataDir, String resultsDir) throws IOException {
        this.dataDir = dataDir;
        this.resultsDir = resultsDir;

        // Hadoop configuration
        configuration = new Configuration();
        configuration.addResource("/usr/local/hadoop/hadoop-3.3.0/etc/hadoop/core-site.xml");
        configuration.addResource("/usr/local/hadoop/hadoop-3.3.0/etc/hadoop/hdfs-site.xml");
        configuration.addResource("/usr/local/hadoop/hadoop-3.3.0/etc/hadoop/mapred-site.xml");
        configuration.addResource("/usr/local/hadoop/hadoop-3.3.0/etc/hadoop/yarn-site.xml");
        configuration.set("mapreduce.map.log.level", "DEBUG");
        configuration.set("mapreduce.reduce.log.level", "DEBUG");
        //configuration.setInt("mapred.reduce.tasks", 5);
        // MapReduce Job
        ratingsJob = Job.getInstance(configuration, "ratingsJob");
        this.setJobInfo();
    }

    /**
     * Set job information
     *
     * @throws IOException
     */
    public void setJobInfo() throws IOException {
        // set Input path
        FileInputFormat.addInputPath(ratingsJob, new Path(dataDir));
        // set the Input Data Format
        ratingsJob.setInputFormatClass(TextInputFormat.class);
        // set Mapper class
        ratingsJob.setMapperClass(RatingsMapper.class);
        // set Reducer class
        ratingsJob.setReducerClass(RatingsReducer.class);
        // set the Jar file
        ratingsJob.setJarByClass(com.company.MovieRatings.class);
        // set Output path
        FileOutputFormat.setOutputPath(ratingsJob, new Path(resultsDir));
        // set the Output Data Format
        ratingsJob.setOutputFormatClass(TextOutputFormat.class);
        // Set the Output Key and Value Class
        ratingsJob.setOutputKeyClass(Text.class);
        ratingsJob.setOutputValueClass(IntWritable.class);
        ratingsJob.setNumReduceTasks(5);
    }

    /**
     * Start job
     *
     * @throws InterruptedException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void startJob() throws InterruptedException, IOException, ClassNotFoundException {
        ratingsJob.waitForCompletion(true);
    }

    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {

        try {
            // instantiate ratings
            MovieRatings ratings = new MovieRatings(args[0], args[1]);
            // set the job info
            ratings.setJobInfo();
            // start the job
            ratings.startJob();

        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}


