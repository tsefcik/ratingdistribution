package com.company;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;

public class RatingsMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    Logger logger = Logger.getLogger(RatingsMapper.class);
    // IntWritable for our count
    IntWritable one = new IntWritable(1);
    // Text for the itemId
    Text itemId = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        logger.debug("DEBUG - IN MAPPER SETUP");
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws InterruptedException, IOException {
        logger.debug("DEBUG - PROCESS A RECORD - MAPPER");
        // get the line from the data file
        String line = value.toString();
        // split the line by each tab separating data
        String[] arrayLine = line.split("\t");
        // set the itemId to the itemId from the data file
        itemId.set(arrayLine[2]);
        // write the itemId to the context and place a count of one for it
        context.write(itemId, one);
        }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        logger.debug("DEBUG - IN MAPPER CLEANUP");
    }
}


