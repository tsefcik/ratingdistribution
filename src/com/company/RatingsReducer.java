package com.company;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

import java.io.IOException;

public class RatingsReducer extends Reducer<Text, IntWritable, Text, IntWritable>{
    Logger logger = Logger.getLogger(RatingsReducer.class);
    // initialize i
    int i = 0;
    // instantiate count
    IntWritable count = new IntWritable();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        logger.debug("DEBUG - IN REDUCER SETUP");
    }

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws InterruptedException, IOException {
        logger.debug("DEBUG - PROCESS A RECORD - REDUCER");
        i = 0;
        // loop through lines from the map
        for (IntWritable val : values) {
            // increase i by 1
            i = i + 1;
        }
        // set the count variable with int i
        count.set(i);
        // write the new count with the corresponding key
        context.write(key, count);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        logger.debug("DEBUG - IN REDUCER CLEANUP");
    }
}
